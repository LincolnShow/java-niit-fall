package niit.fall.rover.programmable;

public interface ProgramFileAware {
    void executeProgramFile(String path);
}

package niit.fall.rover.command;

import niit.fall.rover.Point;
import niit.fall.rover.Rover;
import niit.fall.rover.constants.Direction;

public class LandCommand implements RoverCommand {
    protected Rover rover = null;
    protected Point position = null;
    protected Direction direction = null;

    public LandCommand(Rover rover, Point position, Direction direction) {
        this.rover = rover;
        this.position = position;
        this.direction = direction;
    }

    @Override
    public void execute() {
        rover.land(position, direction);
    }

    @Override
    public String toString() {
        return "Land at " + position.toString() + " heading " + direction.name();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LandCommand)) return false;

        LandCommand that = (LandCommand) o;

        if (rover != null ? !rover.equals(that.rover) : that.rover != null) return false;
        if (position != null ? !position.equals(that.position) : that.position != null) return false;
        return direction == that.direction;

    }

    @Override
    public int hashCode() {
        int result = rover != null ? rover.hashCode() : 0;
        result = 31 * result + (position != null ? position.hashCode() : 0);
        result = 31 * result + (direction != null ? direction.hashCode() : 0);
        return result;
    }
}

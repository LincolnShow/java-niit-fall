package niit.fall.rover;

import niit.fall.rover.constants.CellState;

public class GroundVisor {
    protected Ground ground = null;

    public GroundVisor(Ground ground) {
        this.ground = ground;
    }

    public boolean hasObstacles(Point p) throws OutOfGroundException {
        try {
            return ground.getCell(p.getX(), p.getY()).getState().equals(CellState.OCCUPIED);
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new OutOfGroundException();
        }
    }
}

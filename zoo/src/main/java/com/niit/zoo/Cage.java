package com.niit.zoo;

public class Cage {
    private int capacity;
    private Animal inhabitant;

    public Cage(int i) {
        capacity = i;
    }
    public boolean putAnimal(Animal animal) {
        if(capacity >= animal.getSize()){
            inhabitant = animal;
            return true;
        }
        return false;
    }
    public Animal whoLivesHere() {
        return inhabitant;
    }
    public boolean releaseInhabitant() {
        if(inhabitant.getSize() <= capacity){
            inhabitant = null;
            return true;
        }
        return false;
    }
}

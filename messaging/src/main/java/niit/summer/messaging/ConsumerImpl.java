package niit.summer.messaging;

public class ConsumerImpl implements Consumer {
    private int i = -1;
    private int msgCount = 0;
    public ConsumerImpl(int i) {
        this.i = i;
    }

    @Override
    public boolean consume() {
        try {
            Message message;
            do{
                ++msgCount;
            } while((message = MessageBusImpl.getInstance().take()).getPayload() != i);
            System.out.println("Consumer got message " + message.getPayload() + " and stopped working. MessageBus transferred "+
                    MessageBusImpl.getInstance().getTransferred() + " messages. Consumer received " + msgCount + " messages.");
            return true;
        }catch (InterruptedException e){
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void run() {
        consume();
    }
}

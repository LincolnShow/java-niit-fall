package niit.fall.ui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ImageButton extends JButton {
    private boolean lastButton = false;

    public ImageButton(Image image) {
        super(new ImageIcon(image));
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                super.mouseEntered(e);
                setBorder(BorderFactory.createLineBorder(Color.YELLOW));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                super.mouseExited(e);
                setBorder(BorderFactory.createLineBorder(Color.GRAY));
            }
        });

    }

    public ImageButton() {
        super();
        lastButton = true;
    }

    public boolean isLastButton() {
        return lastButton;
    }
}

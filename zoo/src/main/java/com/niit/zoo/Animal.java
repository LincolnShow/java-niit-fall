package com.niit.zoo;
import com.niit.zoo.Player;

import java.net.Socket;

public abstract class Animal {
    protected String name;
    protected int weight;
    protected String soundFile;

    public Animal(String name, int weight){
        this.name = name;
        this.weight = weight;
    }
    public int getWeight() {
        return weight;
    }
    public String getSoundFile() {
        return soundFile;
    }
    public String getName(){ return name; }
    public int getSize(){ return weight; }

    public void makeSound(){
        Player.play(soundFile);
    }
    public abstract void eat(Food food);
}

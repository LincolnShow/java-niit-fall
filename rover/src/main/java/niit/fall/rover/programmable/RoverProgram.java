package niit.fall.rover.programmable;

import niit.fall.rover.command.RoverCommand;

import java.util.*;

public class RoverProgram {
    public static final String LOG = "log";
    public static final String STATS = "stats";
    public static final String SEPARATOR = "===";

    public static final String OPTION_ON = "on";
    public static final String MOVE = "move";
    public static final String TURN = "turn";
    public static final String LIFT = "lift";
    public static final String LAND = "land";

    protected Map<String, Object> settings = new HashMap<>();
    protected List<RoverCommand> commands = new LinkedList<>();

    public List<RoverCommand> getCommands() {
        return Collections.unmodifiableList(commands);
    }

    public Map<String, Object> getSettings() {
        return Collections.unmodifiableMap(settings);
    }

    public void setCommands(List<RoverCommand> commands) {
        this.commands = commands;
    }

    public void setSettings(Map<String, Object> settings) {
        this.settings = settings;
    }
}

package niit.fall.rover.constants;

public enum CellState {
    FREE, //свободная ячейка
    OCCUPIED //занятая ячейка
}

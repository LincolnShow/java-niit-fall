package niit.summer.messaging;

import java.util.Random;

public class ProducerImpl implements Producer {
    Random random = new Random();

    @Override
    public void produce() {
        try {
            MessageBusImpl.getInstance().put(new Message(random.nextInt(100)));
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        while (true){
            produce();
        }
    }
}

package niit.fall.rover.programmable;

import niit.fall.rover.GroundCell;
import niit.fall.rover.GroundVisor;
import niit.fall.rover.Point;
import niit.fall.rover.Rover;
import niit.fall.rover.command.RoverCommand;
import niit.fall.rover.stats.SimpleRoverStatsModule;

import java.util.List;
import java.util.Map;

/**
 * Этот класс должен уметь все то, что умеет обычный Rover, но при этом он еще должен уметь выполнять программы,
 * содержащиеся в файлах
 */
public class ProgrammableRover extends Rover implements ProgramFileAware {
    protected SimpleRoverStatsModule statsModule = null;
    protected RoverProgram roverProgram = null;

    public ProgrammableRover(GroundVisor visor, SimpleRoverStatsModule statsModule) {
        super(visor);
        this.statsModule = statsModule;
    }

    @Override
    public void executeProgramFile(String path) {
        RoverCommandParser parser = new RoverCommandParser(this, path);
        roverProgram = parser.getProgram();
        boolean needStats = (boolean) roverProgram.getSettings().get(RoverProgram.STATS);
        Point lastPosition = currentPosition;;
        for(RoverCommand command : roverProgram.getCommands()){
            command.execute();
            lastPosition = currentPosition;
            if(needStats && !isAirborne && !lastPosition.equals(currentPosition)){
                    statsModule.registerPosition(lastPosition);
            }
        }

    }

    public Map<String, Object> getSettings() {
        return roverProgram.getSettings();
    }
}

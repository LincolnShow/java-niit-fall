package niit.summer.messaging;
import java.util.concurrent.LinkedBlockingQueue;

public class MessageBusImpl implements MessageBus {
    private static LinkedBlockingQueue<Message> queue;
    private static MessageBusImpl busInstance;
    private static int transferred;

    private MessageBusImpl(int capacity){
        queue = new LinkedBlockingQueue<>(capacity);
        transferred = 0;
    }
    public static synchronized MessageBusImpl getInstance(){
        if(busInstance == null){
            busInstance = new MessageBusImpl(10);
        }
        return busInstance;
    }

    @Override
    public void put(Message message) throws InterruptedException {
        queue.put(message);
    }

    @Override
    public Message take() throws InterruptedException {
        ++transferred;
        return queue.take();
    }

    @Override
    public int queueSize() {
        return queue.size();
    }

    @Override
    public int getTransferred() {
        return transferred;
    }
}

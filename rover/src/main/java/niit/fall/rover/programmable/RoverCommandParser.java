package niit.fall.rover.programmable;

import niit.fall.rover.Point;
import niit.fall.rover.Rover;
import niit.fall.rover.command.*;
import niit.fall.rover.constants.Direction;

import java.io.*;
import java.util.*;

public class RoverCommandParser {
    protected Rover rover = null;
    protected String filepath = null;

    public RoverCommandParser(Rover rover, String filepath) {
        this.rover = rover;
        this.filepath = filepath;
    }

    public RoverProgram getProgram() {
        RoverProgram result = new RoverProgram();
        List<RoverCommand> commands = new LinkedList<>();
        Map<String, Object> settings = new HashMap<>();
        String line;
        String[] split;
        boolean needLog = false;
        try (BufferedReader br = new BufferedReader(new FileReader(this.getClass().getResource(filepath).getFile()))) {
            //Getting settings
            while ((line = br.readLine()) != null) {
                split = line.split(" ");
                if (split[0].equals(RoverProgram.SEPARATOR)) {
                    break;
                }
                settings.put(split[0], split[1].equals(RoverProgram.OPTION_ON));
            }
            if (settings.containsKey(RoverProgram.LOG) && settings.get(RoverProgram.LOG).equals(Boolean.TRUE)) {
                needLog = true;
            }
            //Getting commands
            RoverCommand command = null;
            while ((line = br.readLine()) != null) {
                split = line.split(" ");
                switch (split[0]) {
                    case RoverProgram.MOVE:
                        command = new MoveCommand(rover);
                        break;
                    case RoverProgram.TURN:
                        command = new TurnCommand(rover, Direction.valueOf(split[1].toUpperCase()));
                        break;
                    case RoverProgram.LIFT:
                        command = new LiftCommand(rover);
                        break;
                    case RoverProgram.LAND:
                        Point landingPoint = new Point(Integer.parseInt(split[1]), Integer.parseInt(split[2]));
                        Direction landingDirection = Direction.valueOf(split[3].toUpperCase());
                        command = new LandCommand(rover, landingPoint, landingDirection);
                        break;
                }
                if (needLog) {
                    commands.add(new LoggingCommand(command));
                } else {
                    commands.add(command);
                }
            }
            result.setCommands(commands);
            result.setSettings(settings);
        } catch (IOException | NullPointerException e) {
            throw new RoverCommandParserException();
        }
        return result;
    }
}

package niit.fall.ui;

import javax.swing.*;
import java.util.List;

public class GameController {
    JPanel gamePanel = null;
    public GameController(JPanel panel) {
        gamePanel = panel;
    }

    public void checkSolution(List<ImageButton> field) {
        for(Integer i = 0; i < field.size(); ++i){
            if(field.get(i).getClientProperty("position") != i){
                return;
            }
        }
        gamePanel.removeAll();
    }
}

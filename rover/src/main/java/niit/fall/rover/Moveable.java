package niit.fall.rover;

public interface Moveable {
    void move();
}

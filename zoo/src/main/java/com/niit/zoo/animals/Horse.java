package com.niit.zoo.animals;

public class Horse extends Herbivore {
    public Horse(String name, int weight) {
        super(name, weight);
        this.soundFile = "horse.wav";
    }
}

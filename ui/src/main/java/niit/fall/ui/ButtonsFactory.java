package niit.fall.ui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class ButtonsFactory {

    public ImageButton createImageButton(Image image, int position, ActionListener actionListener) {
        ImageButton result = new ImageButton(image);
        result.putClientProperty("position", position);
        result.addActionListener(actionListener);
        return result;
    }

    public ImageButton createLastButton(int position) {
        ImageButton result = new ImageButton();
        result.putClientProperty("position", position);
        result.setContentAreaFilled(false);
        result.setBorderPainted(false);
        //result.setBorder(BorderFactory.createLineBorder(Color.GREEN));
        return result;
    }
}

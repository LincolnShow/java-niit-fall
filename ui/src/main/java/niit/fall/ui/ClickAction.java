package niit.fall.ui;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.Collections;
import java.util.List;

public class ClickAction extends AbstractAction {
    private GameController controller = null;
    private List<ImageButton> buttons = null;
    private JPanel panel = null;

    public ClickAction(JPanel panel, List<ImageButton> buttons, GameController controller) {
        this.controller = controller;
        this.buttons = buttons;
        this.panel = panel;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        ImageButton caller = (ImageButton) e.getSource();
        if(checkNeighbours(buttons.indexOf(caller), getLastButtonIndex())){
            Collections.swap(buttons, buttons.indexOf(caller), getLastButtonIndex());
            panel.removeAll();
            for(ImageButton b : buttons){
                panel.add(b);
            }
            panel.validate();
        }
        controller.checkSolution(buttons);
    }
    private boolean checkNeighbours(int pos, int pos2){
        //RIGHT EDGE CHECK
        if((((pos+1) % (UIForm.COLS) != 0) && pos+1 == pos2)||
                //LEFT EDGE CHECK
                (((pos) % (UIForm.COLS) != 0) && pos-1 == pos2)||
                //UP & DOWN CHECKS
                pos+UIForm.COLS == pos2 ||
                pos-UIForm.COLS == pos2){
            return true;
        }
        return false;
    }
    public int getLastButtonIndex(){
        for(ImageButton i : buttons) {
            if (i.isLastButton()){
                return buttons.indexOf(i);
            }
        }
        return -1;
    }
}

package com.niit.zoo.animals;

import com.niit.zoo.Animal;
import com.niit.zoo.Food;

public class Omnivore extends Animal {
    public Omnivore(String name, int weight) {
        super(name, weight);
    }
    @Override
    public void eat(Food food) {
        if(food.isFresh()){
            this.weight += food.getNutricity();
        } else {
            this.weight -= food.getNutricity();
        }
    }
}

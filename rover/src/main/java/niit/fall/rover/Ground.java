package niit.fall.rover;

public class Ground {
    protected GroundCell[][] landscape;

    public Ground(int sizeY, int sizeX) {
        landscape = new GroundCell[sizeY][sizeX];
    }

    public void initialize(GroundCell... cells) {
        if (cells.length < (landscape[0].length * landscape.length)) {
            throw new IllegalArgumentException();
        }
        int i = 0;
        //ROWS FIRST
        for (GroundCell[] line : landscape) {
            for (int q = 0; q < line.length; ++q) {
                line[q] = cells[i++];
            }
        }
    }

    public GroundCell getCell(int posX, int posY) throws OutOfGroundException {
        try {
            return landscape[posY][posX];
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new OutOfGroundException();
        }
    }
}

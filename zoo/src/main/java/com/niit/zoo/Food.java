package com.niit.zoo;

public abstract class Food {
    private int nutricity;
    private boolean fresh = true;

    public Food(int nutricity){
        if(nutricity > 0){
            this.nutricity = nutricity;
        }
        else{
            this.nutricity = 0;
        }
    }
    public void setFresh(boolean fresh) {
        this.fresh = fresh;
    }
    public int getNutricity() {
        return nutricity;
    }
    public boolean isFresh() {
        return fresh;
    }
}

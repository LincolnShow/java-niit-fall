package niit.fall.ui;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class UIForm extends JFrame {
    public final static int ROWS = 4;
    public final static int COLS = 3;
    private JPanel buttonsPanel = new JPanel();
    private JPanel contentPanel = new JPanel();
    private GameController gc = null;
    private ImageProcessor ip = new ImageProcessor();

    public void initUI() {
        gc = new GameController(buttonsPanel);
        setContentPane(contentPanel);
        add(buttonsPanel);
        buttonsPanel.setLayout(new GridLayout(ROWS, COLS));

        ButtonsFactory buttonsFactory = new ButtonsFactory();
        List<ImageButton> buttons = new ArrayList<>(ROWS*COLS);
        ClickAction clickAction = new ClickAction(buttonsPanel, buttons, gc);
        Image[][] splitImage = ip.cutImage(this.getClass().getResource("duke.png"), ROWS, COLS);
        int pos = 0;
        for(int r = 0; r < splitImage.length; ++r){
            for(int c = 0; c < splitImage[r].length; ++c){
                buttons.add(buttonsFactory.createImageButton(splitImage[r][c], pos++, clickAction));
            }
        }
        Collections.shuffle(buttons);
        buttons.set((ROWS*COLS)-1,buttonsFactory.createLastButton(pos));
        for(ImageButton b : buttons){
            buttonsPanel.add(b);
        }
        pack();
        setTitle("Puzzle");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        setVisible(true);
    }

    public JPanel getButtonsPanel() {
        return buttonsPanel;
    }
}

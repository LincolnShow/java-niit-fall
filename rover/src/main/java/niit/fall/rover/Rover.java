package niit.fall.rover;

import niit.fall.rover.constants.Direction;

public class Rover implements Moveable, Turnable, Landable, Liftable {
    protected GroundVisor visor = null;
    protected Point currentPosition = null;
    protected Direction direction = null;
    protected boolean isAirborne = false;

    public Rover(GroundVisor visor) {
        this.visor = visor;
        currentPosition = new Point(0, 0);
        try {
            isAirborne = visor.hasObstacles(currentPosition);
        } catch (OutOfGroundException e) {
            isAirborne = true;
        }
        direction = Direction.SOUTH;
    }

    @Override
    public void lift() {
        if (!isAirborne) {
            isAirborne = true;
            direction = null;
            currentPosition = null;
        }
    }

    @Override
    public void move() {
        if (!isAirborne) {
            try {
                Point front = getFront();
                if (!visor.hasObstacles(front)) {
                    currentPosition = front;
                }
            } catch (OutOfGroundException e) {
                this.lift();
            }
        }
    }

    @Override
    public void land(Point position, Direction direction) {
        if (isAirborne) {
            try {
                if (!visor.hasObstacles(position)) {
                    this.currentPosition = position;
                    this.direction = direction;
                    this.isAirborne = false;
                }
            } catch (OutOfGroundException e) {
            }
        }
    }

    @Override
    public void turnTo(Direction direction) {
        this.direction = direction;
    }

    public Point getCurrentPosition() {
        return currentPosition;
    }

    public Direction getDirection() {
        return direction;
    }

    public boolean isAirborne() {
        return isAirborne;
    }

    public Point getFront() {
        int y = currentPosition.getY();
        int x = currentPosition.getX();

        switch (direction) {
            case NORTH:
                return new Point(x, y - 1);
            case EAST:
                return new Point(x + 1, y);
            case SOUTH:
                return new Point(x, y + 1);
            case WEST:
                return new Point(x - 1, y);
            default:
                return null;
        }
    }
}

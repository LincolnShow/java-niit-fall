package niit.fall.rover.command;

import niit.fall.rover.Rover;

public class LiftCommand implements RoverCommand {
    protected Rover rover = null;

    public LiftCommand(Rover rover) {
        this.rover = rover;
    }

    @Override
    public void execute() {
        rover.lift();
    }

    @Override
    public String toString() {
        return "Rover lifted";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LiftCommand)) return false;

        LiftCommand that = (LiftCommand) o;

        return rover != null ? rover.equals(that.rover) : that.rover == null;

    }

    @Override
    public int hashCode() {
        return rover != null ? rover.hashCode() : 0;
    }
}

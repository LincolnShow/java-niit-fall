package com.niit.zoo.animals;

public class Hyena extends Omnivore {
    public Hyena(String name, int weight) {
        super(name, weight);
        this.soundFile = "hyena.wav";
    }
}

package com.niit.zoo.animals;

import com.niit.zoo.Animal;
import com.niit.zoo.Food;
import com.niit.zoo.food.Meat;

public class Carnivore extends Animal {
    public Carnivore(String name, int weight) {
        super(name, weight);
    }
    @Override
    public void eat(Food food) {
        if(food.isFresh() && (food instanceof Meat)){
            this.weight += food.getNutricity();
        } else {
            this.weight -= food.getNutricity();
        }
    }
}

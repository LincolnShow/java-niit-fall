package niit.fall.rover.stats;

import niit.fall.rover.Point;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class SimpleRoverStatsModule implements RoverStatsModule {
    protected List<Point> visited = null;

    public SimpleRoverStatsModule() {
        visited = new LinkedList<>();
    }

    @Override
    public void registerPosition(Point position) {
        if (!isVisited(position)) {
            visited.add(position);
        }
    }

    @Override
    public boolean isVisited(Point point) {
        return visited.contains(point);
    }

    @Override
    public Collection<Point> getVisitedPoints() {
        return visited;
    }
}

package niit.fall.rover;

import niit.fall.rover.constants.CellState;

public class GroundCell {
    protected CellState state;

    public GroundCell(CellState state) {
        this.state = state;
    }

    public CellState getState() {
        return state;
    }
}

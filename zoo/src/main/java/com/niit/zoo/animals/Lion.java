package com.niit.zoo.animals;

public class Lion extends Carnivore {
    public Lion(String name, int weight) {
        super(name, weight);
        this.soundFile = "lion.wav";
    }
}

package com.niit.zoo.animals;

import com.niit.zoo.Animal;
import com.niit.zoo.Food;
import com.niit.zoo.food.Vegetable;

public class Herbivore extends Animal {
    public Herbivore(String name, int weight) {
        super(name, weight);
    }
    @Override
    public void eat(Food food) {
        if(food.isFresh() && (food instanceof Vegetable)){
            this.weight += food.getNutricity();
        } else {
            this.weight -= food.getNutricity();
        }
    }
}

package niit.summer.messaging;
public interface Producer extends Runnable {
    void produce();
}

package niit.fall.rover.command;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggingCommand implements RoverCommand {
    protected RoverCommand roverCommand = null;
    private static final Logger LOGGER = LoggerFactory.getLogger(LoggingCommand.class);

    public LoggingCommand(RoverCommand roverCommand) {
        this.roverCommand = roverCommand;
    }

    @Override
    public void execute() {
        roverCommand.execute();
        LOGGER.debug(toString());
    }

    @Override
    public String toString() {
        return roverCommand.toString();
    }
}
